<%@page import="javax.persistence.Entity"%>
<%@page import="br.com.model.Chale"%>
<%@page import="br.com.model.TipoChale"%>
<%@page import="br.com.dao.impl.ChaleDAOImpl"%>
<%@page import="br.com.dao.IChaleDAO"%>
<%@page import="br.com.dao.impl.FuncionarioDAOImpl"%>
<%@page import="br.com.singleton.FactorySingleton"%>
<%@page import="br.com.dao.IFuncionarioDAO"%>
<%@page import="javax.persistence.EntityManager"%>
<%@page import="br.com.model.TipoFuncionario"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="br.com.model.Funcionario"%>
<%@page import="java.util.List"%>

<%@include file="pgs/topo.jsp"%>

<%
	EntityManager em = FactorySingleton.getInstance().createEntityManager();
	IChaleDAO cdao = new ChaleDAOImpl(em);	
	List<Chale> listaC = cdao.listarTodos();
%>

<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="row">

				<!-- EXIBI��O DA LISTA DE Chal�s -->
				<div class="col-lg-8">
					<div class="panel panel-default">
						<div class="panel-heading">
							<i class="fa fa-bell fa-fw"></i> Lista de Chal�s
						</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<div class="dataTable_wrapper">
								<table class="table table-striped table-bordered table-hover"
									id="dataTables-example">
									<thead>
										<tr>
											<th>N�</th>
											<th>Nome</th>
											<th>Valor</th>
											<th>+ Camas</th>
											<th>Alterar</th>
											<th>Deletar</th>
										</tr>
									</thead>
									<tbody>
											  <c:forEach var="chale" items="<%= listaC %>">
											<tr>
												<td style="width: 10%">${chale.numChale }</td>
												<td style="width: 20%">${chale.tipoChale}</td>
												<td style="width: 20%">${chale.valor_diaria }</td>
												<td style="width: 20%">${chale.camaOpcional }</td>
												<td>													
													<a disabled="disabled" href="" class="btn btn-warning">Alterar</a>
												</td>
												<td>	
													<c:url value="ChaleServlet" var="deletar">
														<c:param name="acao" value="excluir"/>
														<c:param name="codigo" value="${chale.IDChale}"/>
													</c:url>
													<a disabled="disabled" href="${deletar }" onclick="return confirm ('Deseja excluir?')" class="btn btn-danger">Excluir</a>
												</td>
												
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->

						</div>
						<!-- /.panel-body -->
					</div>
				</div>

				<!-- CADASTRO INDIVIDUAL DE CHAL�S -->
				<div class="col-lg-4">
					<div class="row">
						<div class="panel panel-default">
							<div class="panel-heading">
								<i class="fa fa-bell fa-fw"></i> Cadastrar Chal�
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body">
								<form class="horizontal-form" action="ChaleServlet"
									method="post">									
									
									<section class="col-md-6">
										<input type="hidden" name="acao" value="alterar">
										<input type="hidden" name="codigo" value="${chale.IDChale}">
										
										<label>N�mero do chal�:</label>								
										<input name="numChale" required class="form-control" type="text"  value="${chale.numChale}">
										
										<label>Tipo Chal� </label>
										  <select name="tipoChale" class="form-control" >
										  		<option value="${chale.tipoChale }">${chale.tipoChale }</option>
										  		<c:forEach items="<%= TipoChale.values() %>" var="state">																						
								                	<c:if test="${chale.tipoChale != state}"><option  value="${state}"> ${state}</option>	</c:if>							                								                
								             </c:forEach> 
								            </select>
								            
								            <label>Valor Chal�</label>
										<input name="valorChale" value="${chale.valor_diaria }" type="text" required class="form-control">
																				
										<label for="cama"> Cama Opcional </label>
										<input required	class="form-control" value="${chale.camaOpcional }" name="camaOpc" type="text">
								            
								            
									</section>
									<section class="col-md-6">
										<div class="row">
										<div class="col-md-6">
										<br><button type="submit" class="btn btn-success">Alterar</button></br></br></br>
										<br>										
										<a href="ChaleServlet?acao=listar" class="btn btn-default">Voltar</a>	
										</div></div>
									</section>
								</form>
							</div>
							<!-- /.panel-body -->
						</div>
					</div>



					<!-- EXIBI��O INDIVIDUAL DE Chal� -->
					<div class="row">
						<div class="panel panel-default">
							<div class="panel-heading">
							
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body">
								<div id="nomeChale" class="col-md-4"></div>
								<div class="col-md-12">
									<ol>

										<li >Honey Moon - Lua de mel, quarto feito para um casal </li>
										<li >Family - Fam�lia, quarto feito para a fam�lia tradicional brasileira
										<br>(Uma cama de casal e uma beliche)</li>
										<li >Big Family - Quarto feito para uma fucking great family
										<br>
										(Tr�s camas de casal, quatro beliches)</li>
										
									
									</ol>
								</div>
							</div>
							<!-- /.panel-body -->
						</div>
					</div>
				</div>

			</div>
			<!-- /.row -->


		</div>
	</div>
</div>

<%@include file="pgs/footer.jsp"%>