<%@page import="br.com.dao.impl.FuncionarioDAOImpl"%>
<%@page import="br.com.singleton.FactorySingleton"%>
<%@page import="br.com.dao.IFuncionarioDAO"%>
<%@page import="javax.persistence.EntityManager"%>
<%@page import="br.com.model.TipoFuncionario"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="br.com.model.Funcionario"%>
<%@page import="java.util.List"%>
<%@page import="br.com.control.FuncionarioServlet"%>
<%@include file="pgs/topo.jsp"%>

<%
	EntityManager em = FactorySingleton.getInstance().createEntityManager();
	IFuncionarioDAO fdao = new FuncionarioDAOImpl(em);
	List<Funcionario> lista = fdao.listarTodos();
	
%>
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="row">

				<!-- EXIBI��O DA LISTA DE FUNCIONARIO -->
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<i class="fa fa-bell fa-fw"></i> Lista de Funcionarios
						</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<div class="dataTable_wrapper">
								<table class="table table-striped table-bordered table-hover"
									id="dataTables-example">
									<thead>
										<tr> 
											<th>#</th>
											<th>Nome</th>
											<th>CPF</th>
											<th>Fun��o</th>
											<th>Alterar</th>
											<th>Deletar</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="func" items="<%=lista %>">
											<tr>
												<td style="width: 10%">${func.idUsuario}</td>
												<td>${func.nome}</td>
												<td>${func.cpf }
												<td>${func.funcao}</td>
												<td>
													<c:url value="funcionarioServlet" var="alterar">
														<c:param name="acao" value="carregar"/>
														<c:param name="codigo" value="${func.idUsuario}"/>
													</c:url>
													<a href="${alterar}" class="btn btn-warning">Alterar</a>
												</td>
												<td>	
													<c:url value="funcionarioServlet" var="deletar">
														<c:param name="acao" value="excluir"/>
														<c:param name="codigo" value="${func.idUsuario}"/>
													</c:url>
													<a href="${deletar}" onclick="return confirm ('Deseja excluir?')" class="btn btn-danger">Excluir</a>
												</td>
												
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->

						</div>
						<!-- /.panel-body -->
					</div>
				</div>

				<!-- CADASTRO INDIVIDUAL DE FUNCIONARIO -->
				<div class="col-lg-6">
					<div class="row">
						<div class="panel panel-default">
							<div class="panel-heading">
								<i class="fa fa-bell fa-fw"></i> Cadastrar Funcionarios
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body">
								<form if="form1" class="horizontal-form" action="funcionarioServlet"
									method="post">
									<input type="hidden" name="acao" value="cadastrar">
									<section class="col-md-6">
										
										<label for="nome"> Nome </label> 
										<input required	class="form-control" name="nome" type="text"> 
										
										<label for="sobreNome"> Sobrenome </label>
										<input required	class="form-control" name="sobreNome" type="text">
										
										
										<label for="rg"> RG:</label>
										<input required class="form-control"  name="rg" type="text" id="rg" size="30" maxlength="12" onKeyPress="MascaraRG(form1.rg);"> 
										
										<label for="cpf"> CPF:</label> 
										<input required class="form-control" onkeypress="formatar('###.###.###-##', this);"  name="cpf" type="text">
										
										<label for="celular"> Celular: </label>
										<input required	class="form-control" name="celular" type="text"> 
										
										<label for="salario"> Sal�rio: </label> 
										<input required	class="form-control" name="salario" type="text"> 
										
										<label for="rua"> Rua: </label> 
										<input required class="form-control" name="rua" type="text"> 
										
										<label for="numero"> N�mero: </label> 
										<input required class="form-control" name="numero" type="text"> 
										

										<label for="cep"> CEP: </label> 
										<input required class="form-control"  onkeypress="return MM_formtCep(event,this,'#####-###');" size="10" maxlength="9">
									
									
									</section>
									<section class="col-md-6">
										<label for="dataNascimento">Data de nascimento: </label> 
										<input class="form-control" name="dataNascimento" type="date">
										
										<label for="telefoneResidencial"> Telefone: </label> 
										<input class="form-control" name="telefoneResidencial" type="text">
										
										<label> Func�o: </label> 
										<select name="funcao" class="form-control" >
											<c:forEach items="<%= TipoFuncionario.values() %>" var="state">												
								                <option  value="${state}"> ${state}</option>								                								                
								             </c:forEach>  
							            </select>									
										
										
										<label for="dataAdmissao">Data admiss�o: </label> 
										<input class="form-control" name="dataAdmissao" type="date">
										
										<label for="bairro"> Bairro: </label> 
										<input required class="form-control" name="bairro" type="text"> 
										
										<label for="cidade"> Cidade: </label> 
										<input required	class="form-control" name="cidade" type="text"> 
										
										<label for="estado"> Estado: </label> 
										<input required	class="form-control" name="estado" type="text"> 
										
										<br>
										<button type="submit" class="btn btn-success">
											Cadastrar</button>
									</section>
								</form>
							</div>
							<!-- /.panel-body -->
						</div>
					</div>



					<!-- EXIBI��O INDIVIDUAL DE FUNCIONARIO -->
					<div class="row">
						<div class="panel panel-default">
							<div class="panel-heading">
								<i class="fa fa-bell fa-fw"></i> <span id="nomeEmpresa">
								</span>
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body">
								<div id="logoEmpresa" class="col-md-4"></div>
								<div class="col-md-8">
									<ul>

										<li id=""></li>
										<li id=""></li>
										<li id=""></li>
									</ul>
								</div>
							</div>
							<!-- /.panel-body -->
						</div>
					</div>
				</div>

			</div>
			<!-- /.row -->


		</div>
	</div>
</div>

<%@include file="pgs/footer.jsp"%>