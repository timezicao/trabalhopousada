<%@page import="br.com.dao.impl.FuncionarioDAOImpl"%>
<%@page import="br.com.singleton.FactorySingleton"%>
<%@page import="br.com.dao.IFuncionarioDAO"%>
<%@page import="javax.persistence.EntityManager"%>
<%@page import="br.com.model.TipoFuncionario"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="br.com.model.Funcionario"%>
<%@page import="java.util.List"%>
<%@page import="br.com.control.FuncionarioServlet"%>
<%@include file="pgs/topo.jsp"%>

<%
	EntityManager em = FactorySingleton.getInstance().createEntityManager();
	IFuncionarioDAO fdao = new FuncionarioDAOImpl(em);
	List<Funcionario> lista = fdao.listarTodos();
	
%>
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="row">

				<!-- EXIBI��O DA LISTA DE FUNCIONARIO -->
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<i class="fa fa-bell fa-fw"></i> RESERVA
						</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<div class="dataTable_wrapper">
								<table class="table table-striped table-bordered table-hover"
									id="dataTables-example">
									<thead>
										<tr> 
											<th>#</th>
											<th>Nome</th>
											<th>CPF</th>
											<th>Entrada</th>
											<th>Sa�da</th>
											<th>Alterar</th>
											<th>Deletar</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="func" items="<%=lista %>">
											<tr>
												<td style="width: 10%">${func.idUsuario}</td>
												<td>${func.nome}</td>
												<td>${func.cpf }
												<td>${func.funcao}</td>
												<td>
													<c:url value="funcionarioServlet" var="alterar">
														<c:param name="acao" value="carregar"/>
														<c:param name="codigo" value="${func.idUsuario}"/>
													</c:url>
													<a href="${alterar}" class="btn btn-warning">Alterar</a>
												</td>
												<td>	
													<c:url value="funcionarioServlet" var="deletar">
														<c:param name="acao" value="excluir"/>
														<c:param name="codigo" value="${func.idUsuario}"/>
													</c:url>
													<a href="${deletar}" onclick="return confirm ('Deseja excluir?')" class="btn btn-danger">Excluir</a>
												</td>
												
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->

						</div>
						<!-- /.panel-body -->
					</div>
				</div>

				<!-- CADASTRO INDIVIDUAL DE FUNCIONARIO -->
				<div class="col-lg-6">
					<div class="row">
						<div class="panel panel-default">
							<div class="panel-heading">
								<i class="fa fa-bell fa-fw"></i> EFETUAR RESERVA
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body">
								<form class="horizontal-form" action="funcionarioServlet"
									method="post">
									<input type="hidden" name="acao" value="cadastrar">
									<section class="col-md-6">
										
										<label for="nome"> Nome </label> 
										<input required	class="form-control" name="nome" type="text"> 
										
										<label for="cpf"> CPF:</label> 
										<input required class="form-control" maxlength="14" onkeypress="formatar('###.###.###-##', this);"  name="cpf" type="text">
										 
										
										<label for="Datadeentrada"> Data de entrada: </label> 
										<input required class="form-control" name="dtEntrada" type="date"> 
										
										<label for="Datadesaida"> Data de sa�da: </label> 
										<input required class="form-control" name="dtSaida" type="date"> 
										
										<label for="TipoDePagamento"> Tipo de pagamento: </label> 
										<input required class="form-control" name="tpPagamento" type="text">
										
									</section>
									<section class="col-md-6">
										<label for="valor">Valor: </label> 
										<input class="form-control" name="valor" type="text">
										
										<label for="transporte">Transporte </label> </br>
										<input type="checkbox" name="transporteIda" value="ida">Transporte ida</br>
										<input type="checkbox" name="transporteVolta" value="volta">Transporte volta</br>
										</br></br><button type="submit" class="btn btn-success"> Cadastrar</button></br>
										</br></br><button type="reset" class="btn  btn-danger"> Limpar</button></br>
										
									</section>
								</form>
							</div>
							<!-- /.panel-body -->
						</div>
					</div>



		
				
				</div>

			</div>
			<!-- /.row -->


		</div>
	</div>
</div>

<%@include file="pgs/footer.jsp"%>