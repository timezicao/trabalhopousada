<%@page import="br.com.model.TipoFuncionario"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="br.com.model.Funcionario"%>
<%@page import="br.com.model.Endereco"%>
<%@page import="java.util.List"%>
<%@page import="br.com.control.FuncionarioServlet"%>
<%@include file="pgs/topo.jsp"%>


<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="row">

				<!-- EXIBI��O DA LISTA DE FUNCIONARIO -->
				<div class="col-lg-8">
					<div class="panel panel-default">
						<div class="panel-heading">
							<i class="fa fa-bell fa-fw"></i> Lista de Funcionarios
						</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<div class="dataTable_wrapper">
								<table class="table table-striped table-bordered table-hover"
									id="dataTables-example">
									<thead>
										<tr>
											<th>#</th>
											<th>Nome</th>
											<th>CPF</th>
											<th>Fun��o</th>
											<th>Alterar</th>
											<th>Deletar</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="func" items="${listaFuncionarios}">
											<tr>
												<td style="width: 10%">${func.idUsuario}</td>
												<td>${func.nome}</td>
												<td>${func.cpf }
												<td>${func.funcao}</td>
												<td>
													<c:url value="funcionarioServlet" var="alterar">
														<c:param name="acao" value="carregar"/>
														<c:param name="codigo" value="${func.idUsuario}"/>
													</c:url>
													<button disabled="disabled" class="btn btn-warning">Alterar</button>
												</td>
												<td>	
													<c:url value="funcionarioServlet" var="deletar">
														<c:param name="acao" value="excluir"/>
														<c:param name="codigo" value="${func.idUsuario}"/>
													</c:url>
													<a href="${deletar}" onclick="return confirm ('Deseja excluir?')" class="btn btn-danger">Excluir</a>
												</td>
												
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->

						</div>
						<!-- /.panel-body -->
					</div>
				</div>

				<!-- CADASTRO INDIVIDUAL DE FUNCIONARIO -->
				<div class="col-lg-4">
					<div class="row">
						<div class="panel panel-default">
							<div class="panel-heading">
								<i class="fa fa-bell fa-fw"></i> Cadastrar Funcionarios
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body">

								<form class="horizontal-form" action="funcionarioServlet" method="post">
									<input type="hidden" name="acao" value="alterar">
									<section class="col-md-6">
										<input type="hidden" name="codigo" value="${funcionario.idUsuario}"/>
										<input type="hidden" name="codEndereco" value="${funcionario.endereco.idEndereco}"/>
										
										<label for="nome"> Nome </label> 
										<input class="form-control" name="nome" type="text" value="${funcionario.nome}">
										<label for="sobreNome"> Sobrenome </label> 
										<input class="form-control" name="sobreNome" type="text" value="${funcionario.sobreNome }"> 
										<label for="rg"> RG:</label> 
										<input class="form-control" name="rg" type="text" value="${funcionario.rg}"> 
										<label for="cpf"> CPF:</label> 
										<input class="form-control" name="cpf" type="text" value="${funcionario.cpf}">
										<label for="celular"> Celular: </label> 
										<input class="form-control" name="celular" type="text" value="${funcionario.celular}"> 
										<label for="salario"> Sal�rio: </label> 
										<input class="form-control" name="salario" type="text" value="${funcionario.salario}"> 
										<label for="rua"> Rua: </label> 
										<input class="form-control" name="rua" type="text" value="${funcionario.endereco.rua}"> 
										<label for="numero">N�mero: </label> 
										<input class="form-control" name="numero" type="text" value="${funcionario.endereco.numero}">
										<label for="cep"> CEP: </label>
										<input class="form-control" name="cep" type="text" value="${funcionario.endereco.cep}">
									</section>
									<section class="col-md-6">
										<label for="dataNascimento">Data de nascimento: </label> 
										<input class="form-control" name="dataNascimento" type="text" 
										 value='<fmt:formatDate value="${funcionario.dataNascimento}" pattern="dd/MM/yyyy"/>'					
										readonly="readonly">
										<label for="telefoneResidencial"> Telefone: </label> 
										<input class="form-control" name="telefoneResidencial" type="text" value="${funcionario.telefoneResidencial}">
										<label for="funcao"> Func�o: </label> 
										<label> Func�o: </label> 
										<select name="funcao" class="form-control" >
											<option value="${funcionario.funcao }">${funcionario.funcao }</option>
											<c:forEach items="<%= TipoFuncionario.values() %>" var="state">																						
								                <c:if test="${funcionario.funcao != state}"><option  value="${state}"> ${state}</option>	</c:if>							                								                
								             </c:forEach>  
							            </select>									
										
										<label for="dataAdmissao">Data admiss�o: </label> 
										<input class="form-control" name="dataAdmissao" type="text" 
										value='<fmt:formatDate value="${funcionario.dataAdmissao}" pattern="dd/MM/yyyy"/>'					
										readonly="readonly">
										<label for="bairro"> Bairro: </label> 
										<input 	class="form-control" name="bairro" type="text" value="${funcionario.endereco.bairro}"> 
										<label	for="cidade"> Cidade: </label> 
										<input 	class="form-control" name="cidade" type="text" value="${funcionario.endereco.cidade}"> 
										<label	for="estado"> Estado: </label> 
										<input 	class="form-control" name="estado" type="text" value="${funcionario.endereco.estado}"> 
										<br>										
										<button type="submit" class="btn btn-warning">Alterar</button>
										<br>
										<br>										
										<a href="funcionarioServlet?acao=listar" class="btn btn-default">Voltar</a>																				
									</section>
								</form>

							</div>
							<!-- /.panel-body -->
						</div>
					</div>



					<!-- EXIBI��O INDIVIDUAL DE FUNCIONARIO -->
					<div class="row">
						<div class="panel panel-default">
							<div class="panel-heading">
								<i class="fa fa-bell fa-fw"></i> <span id="nomeEmpresa">
								</span>
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body">
								<div id="nome" class="col-md-4"></div>
								<div class="col-md-8">
									<ul>

										<li id="nascimento"></li>
										<li id="email"></li>
										<li id="cargo"></li>
									</ul>
								</div>
							</div>
							<!-- /.panel-body -->
						</div>
					</div>
				</div>

			</div>
			<!-- /.row -->


		</div>
	</div>
</div>

<%@include file="pgs/footer.jsp"%>