/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Wifi extends Servico{
   
    private String senha;
    private int QTDDispositivos;
    
	
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public int getQTDDispositivos() {
		return QTDDispositivos;
	}
	public void setQTDDispositivos(int qTDDispositivos) {
		QTDDispositivos = qTDDispositivos;
	}

   
}
