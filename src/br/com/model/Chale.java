/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;


@Entity
@SequenceGenerator(name="codChale",sequenceName="seqChale")
public class Chale {
    
	public Chale(){
		listaReservas = new ArrayList<>();
	}
	
	@Id
	@GeneratedValue(generator="codChale", strategy=GenerationType.AUTO)
    private int IDChale;
	private int numChale;
    private Double Valor_diaria;
    private String camaOpcional;
	private TipoChale tipoChale;
	
	@OneToMany(fetch=FetchType.LAZY,mappedBy="chale",cascade=CascadeType.PERSIST)
    private List<Reserva> listaReservas;
	
	public void addReserva(Reserva reserva){
		listaReservas.add(reserva);
		reserva.setChale(this);
	}
	
    public int getNumChale() {
		return numChale;
	}

	public void setNumChale(int numChale) {
		this.numChale = numChale;
	}

	public String getCamaOpcional() {
		return camaOpcional;
	}

	public void setCamaOpcional(String camaOpcional) {
		this.camaOpcional = camaOpcional;
	}

	public TipoChale getTipoChale() {
		return tipoChale;
	}

	public void setTipoChale(TipoChale tipoChale) {
		this.tipoChale = tipoChale;
	}
    
    public int getIDChale() {
        return IDChale;
    }

    public void setIDChale(int IDChale) {
        this.IDChale = IDChale;
    }

    public Double getValor_diaria() {
        return Valor_diaria;
    }

    public void setValor_diaria(Double Valor_diaria) {
        this.Valor_diaria = Valor_diaria;
    }

	public List<Reserva> getListaReservas() {
		return listaReservas;
	}

	public void setListaReservas(List<Reserva> listaReservas) {
		this.listaReservas = listaReservas;
	}

    
}
