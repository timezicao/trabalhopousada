/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;


@Entity
@SequenceGenerator(name="codCartaoAcesso",sequenceName="seqCartaoAcesso")
public class CartaoAcesso {
    
	@Id
	@GeneratedValue(generator="codCartaoAcesso", strategy=GenerationType.AUTO)
	private int IDCartaoAcesso;
	private int Numero;
    private String Status;
       
    public int getIDCartaoAcesso() {
		return IDCartaoAcesso;
	}

	public void setIDCartaoAcesso(int iDCartaoAcesso) {
		IDCartaoAcesso = iDCartaoAcesso;
	}

	public int getNumero() {
        return Numero;
    }

    public void setNumero(int Numero) {
        this.Numero = Numero;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }
    
    
}
