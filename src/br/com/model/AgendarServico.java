/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.model;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@SequenceGenerator(name="codAgendarServico",sequenceName="seqAgendarServico")
public class AgendarServico {
    
	@Id
	@GeneratedValue(generator="codAgendarServico", strategy=GenerationType.AUTO)
    private int IDAgendamento;
	@Temporal(TemporalType.DATE)
	private Date DTAgendamento;
	@Temporal(TemporalType.TIME)
	private Date HoraAgendamento;
	private int QTDAgendamento;
	private double ValorAgendamento;
	private String Status;
	
	
	public int getIDAgendamento() {
		return IDAgendamento;
	}
	public void setIDAgendamento(int iDAgendamento) {
		IDAgendamento = iDAgendamento;
	}
	public Date getDTAgendamento() {
		return DTAgendamento;
	}
	public void setDTAgendamento(Date dTAgendamento) {
		DTAgendamento = dTAgendamento;
	}
	public Date getHoraAgendamento() {
		return HoraAgendamento;
	}
	public void setHoraAgendamento(Date horaAgendamento) {
		HoraAgendamento = horaAgendamento;
	}
	public int getQTDAgendamento() {
		return QTDAgendamento;
	}
	public void setQTDAgendamento(int qTDAgendamento) {
		QTDAgendamento = qTDAgendamento;
	}
	public double getValorAgendamento() {
		return ValorAgendamento;
	}
	public void setValorAgendamento(double valorAgendamento) {
		ValorAgendamento = valorAgendamento;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}

	
    
}
