/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="codProduto",sequenceName="seqProduto")
public class Produto {
    
	@Id
	@GeneratedValue(generator="codProduto", strategy=GenerationType.AUTO)
    private int IDProduto;
    private int Quantidade;
    private String DescricaoProduto;
    private Double ValorProduto;

    public int getIDProduto() {
        return IDProduto;
    }

    public void setIDProduto(int IDProduto) {
        this.IDProduto = IDProduto;
    }

    public int getQuantidade() {
        return Quantidade;
    }

    public void setQuantidade(int Quantidade) {
        this.Quantidade = Quantidade;
    }

    public String getDescricaoProduto() {
        return DescricaoProduto;
    }

    public void setDescricaoProduto(String DescricaoProduto) {
        this.DescricaoProduto = DescricaoProduto;
    }

    public Double getValorProduto() {
        return ValorProduto;
    }

    public void setValorProduto(Double ValorProduto) {
        this.ValorProduto = ValorProduto;
    }
    
    
}
