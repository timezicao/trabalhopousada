package br.com.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@SequenceGenerator(name="codNotaFiscal", sequenceName="seqNotaFiscal")
public class NotaFiscal {
	
	@Id
	@GeneratedValue(generator="codNotaFiscal", strategy=GenerationType.AUTO)
	private int IDNotaFiscal;
	private int NumeroNotaFiscal;
	private double ValorNotaFical;
	@Temporal(TemporalType.DATE)
	private Date DTNotaFiscal;
	
	public int getIDNotaFiscal() {
		return IDNotaFiscal;
	}
	public void setIDNotaFiscal(int iDNotaFiscal) {
		IDNotaFiscal = iDNotaFiscal;
	}
	public int getNumeroNotaFiscal() {
		return NumeroNotaFiscal;
	}
	public void setNumeroNotaFiscal(int numeroNotaFiscal) {
		NumeroNotaFiscal = numeroNotaFiscal;
	}
	public double getValorNotaFical() {
		return ValorNotaFical;
	}
	public void setValorNotaFical(double valorNotaFical) {
		ValorNotaFical = valorNotaFical;
	}
	public Date getDTNotaFiscal() {
		return DTNotaFiscal;
	}
	public void setDTNotaFiscal(Date dTNotaFiscal) {
		DTNotaFiscal = dTNotaFiscal;
	}
	
	
	
	
	
}
