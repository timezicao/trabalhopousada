package br.com.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Funcionario extends Usuario{
	
	private TipoFuncionario funcao;
	private double salario;
	private int numCNH;
    private char catCNH;
    private int validadeCNH;
    
    @Temporal(TemporalType.DATE)
	private Date dataAdmissao;
	@Temporal(TemporalType.DATE)
	private Date dataDemissao;
	
	public TipoFuncionario getFuncao() {
		return funcao;
	}
	public void setFuncao(TipoFuncionario funcao) {
		this.funcao = funcao;
	}
	public double getSalario() {
		return salario;
	}
	public void setSalario(double salario) {
		this.salario = salario;
	}
	public int getNumCNH() {
		return numCNH;
	}
	public void setNumCNH(int numCNH) {
		this.numCNH = numCNH;
	}
	public char getCatCNH() {
		return catCNH;
	}
	public void setCatCNH(char catCNH) {
		this.catCNH = catCNH;
	}
	public int getValidadeCNH() {
		return validadeCNH;
	}
	public void setValidadeCNH(int validadeCNH) {
		this.validadeCNH = validadeCNH;
	}
	public Date getDataAdmissao() {
		return dataAdmissao;
	}
	public void setDataAdmissao(Date dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}
	public Date getDataDemissao() {
		return dataDemissao;
	}
	public void setDataDemissao(Date dataDemissao) {
		this.dataDemissao = dataDemissao;
	}
	
}
