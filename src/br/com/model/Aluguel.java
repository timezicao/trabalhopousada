/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.model;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@SequenceGenerator(name="codAluguel",sequenceName="seqAluguel")
public class Aluguel {

	@Id
	@GeneratedValue(generator="codAluguel", strategy=GenerationType.AUTO)
    private int IDAluguel;
	@Temporal(TemporalType.DATE)
	private Date DTEntrada;
	@Temporal(TemporalType.DATE)
    private Date DTSaida;
    private double ValorAluguel;
	
    public int getIDAluguel() {
		return IDAluguel;
	}
	public void setIDAluguel(int iDAluguel) {
		IDAluguel = iDAluguel;
	}
	public Date getDTEntrada() {
		return DTEntrada;
	}
	public void setDTEntrada(Date dTEntrada) {
		DTEntrada = dTEntrada;
	}
	public Date getDTSaida() {
		return DTSaida;
	}
	public void setDTSaida(Date dTSaida) {
		DTSaida = dTSaida;
	}
	public double getValorAluguel() {
		return ValorAluguel;
	}
	public void setValorAluguel(double valorAluguel) {
		ValorAluguel = valorAluguel;
	}

    
}
