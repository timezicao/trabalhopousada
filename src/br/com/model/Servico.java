/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="codServico",sequenceName="seqServico")
public class Servico {
    @Id
    @GeneratedValue(generator="codServico", strategy=GenerationType.AUTO)
    private int IDServico;
    private String NomeServico;
    private Double ValorServico;
    private String Descricao;
	
    public int getIDServico() {
		return IDServico;
	}
	public void setIDServico(int iDServico) {
		IDServico = iDServico;
	}
	public String getNomeServico() {
		return NomeServico;
	}
	public void setNomeServico(String nomeServico) {
		NomeServico = nomeServico;
	}
	public Double getValorServico() {
		return ValorServico;
	}
	public void setValorServico(Double valorServico) {
		ValorServico = valorServico;
	}
	public String getDescricao() {
		return Descricao;
	}
	public void setDescricao(String descricao) {
		Descricao = descricao;
	}

    
    
}
