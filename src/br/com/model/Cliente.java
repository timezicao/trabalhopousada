/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Entity
public class Cliente extends Usuario{
    
	public Cliente(){
		listaReservas = new ArrayList<>();
	}
	private String email;
	
	@OneToMany(fetch=FetchType.LAZY,mappedBy="cliente",cascade=CascadeType.PERSIST)
	private List<Reserva> listaReservas;
	
    public void addReserva(Reserva reserva){
    	listaReservas.add(reserva);
    	reserva.setCliente(this);
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String Email) {
        this.email = Email;
    }

	public List<Reserva> getListaReservas() {
		return listaReservas;
	}

	public void setListaReservas(List<Reserva> listaReservas) {
		this.listaReservas = listaReservas;
	}
    
}
