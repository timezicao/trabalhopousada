package br.com.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@SequenceGenerator(name="codConsumoProdutos", sequenceName="seqConsumoProdutos")
public class ConsumoProdutos {

	@Id
	@GeneratedValue(generator="codConsumoProdutos", strategy=GenerationType.AUTO)
	private int IDConsumoProdutos;
	private int QTDConsumoProdutos;
	private double ValorConsumoProdutos;
	@Temporal(TemporalType.DATE)
	private Date DTConsumoProdutos;
	
	public int getIDConsumoProdutos() {
		return IDConsumoProdutos;
	}
	public void setIDConsumoProdutos(int iDConsumoProdutos) {
		IDConsumoProdutos = iDConsumoProdutos;
	}
	public int getQTDConsumoProdutos() {
		return QTDConsumoProdutos;
	}
	public void setQTDConsumoProdutos(int qTDConsumoProdutos) {
		QTDConsumoProdutos = qTDConsumoProdutos;
	}
	public double getValorConsumoProdutos() {
		return ValorConsumoProdutos;
	}
	public void setValorConsumoProdutos(double valorConsumoProdutos) {
		ValorConsumoProdutos = valorConsumoProdutos;
	}
	public Date getDTConsumoProdutos() {
		return DTConsumoProdutos;
	}
	public void setDTConsumoProdutos(Date dTConsumoProdutos) {
		DTConsumoProdutos = dTConsumoProdutos;
	}
	
	
}
