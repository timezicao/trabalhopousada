/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="codTablet",sequenceName="seqTablet")
public class Tablet {
    
	@Id
	@GeneratedValue(generator="codTablet", strategy=GenerationType.AUTO)
    private int IDTablet;
    private String Modelo;
    private String Descricao;
  
    public int getIDTablet() {
        return IDTablet;
    }

    public void setIDTablet(int IDTablet) {
        this.IDTablet = IDTablet;
    }

    public String getModelo() {
        return Modelo;
    }

    public void setModelo(String Modelo) {
        this.Modelo = Modelo;
    }

    public String getDescricao() {
        return Descricao;
    }

    public void setDescricao(String Descricao) {
        this.Descricao = Descricao;
    }

    
}
