/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.model;

import java.sql.Time;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Transporte extends Servico{
    

    private String Destino;
    private Time TempoViagem;

    public String getDestino() {
        return Destino;
    }

    public void setDestino(String Destino) {
        this.Destino = Destino;
    }

	public Time getTempoViagem() {
		return TempoViagem;
	}

	public void setTempoViagem(Time tempoViagem) {
		TempoViagem = tempoViagem;
	}

    
    
}
