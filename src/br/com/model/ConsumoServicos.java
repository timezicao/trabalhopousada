package br.com.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@SequenceGenerator(name="codConsumoServicos", sequenceName="seqConsumoServicos")
public class ConsumoServicos {

	@Id
	@GeneratedValue(generator="codConsumoServicos", strategy=GenerationType.AUTO)
	private int IDConsumoServicos;
	@Temporal(TemporalType.DATE)
	private Date DTConsumoServicos;
	@Temporal(TemporalType.TIME)
	private Date HoraConsumoServicos;
	private int QTDConsumoServicos;
	
	
	public int getIDConsumoServicos() {
		return IDConsumoServicos;
	}
	public void setIDConsumoServicos(int iDConsumoServicos) {
		IDConsumoServicos = iDConsumoServicos;
	}
	public Date getDTConsumoServicos() {
		return DTConsumoServicos;
	}
	public void setDTConsumoServicos(Date dTConsumoServicos) {
		DTConsumoServicos = dTConsumoServicos;
	}
	public Date getHoraConsumoServicos() {
		return HoraConsumoServicos;
	}
	public void setHoraConsumoServicos(Date horaConsumoServicos) {
		HoraConsumoServicos = horaConsumoServicos;
	}
	public int getQTDConsumoServicos() {
		return QTDConsumoServicos;
	}
	public void setQTDConsumoServicos(int qTDConsumoServicos) {
		QTDConsumoServicos = qTDConsumoServicos;
	}
	
	
	
}
