/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.model;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@SequenceGenerator(name="codReserva",sequenceName="seqReserva")
//@Inheritance(strategy=InheritanceType.JOINED)
public class Reserva {
    @Id
	@GeneratedValue(generator="codReserva", strategy=GenerationType.AUTO)
    private int IDReserva;
    @Temporal(TemporalType.DATE)
    private Date DTReserva;
    @Temporal(TemporalType.DATE)
    private Date DTEntrada;
    @Temporal(TemporalType.DATE)
    private Date DTSaida;
    private Double Valor;
    private int TipoPagamento;
    @ManyToOne
    private Chale chale;
    
    @ManyToOne
    private Cliente cliente;
    
    
	public int getIDReserva() {
		return IDReserva;
	}
	public void setIDReserva(int iDReserva) {
		IDReserva = iDReserva;
	}
	public Date getDTReserva() {
		return DTReserva;
	}
	public void setDTReserva(Date dTReserva) {
		DTReserva = dTReserva;
	}
	public Date getDTEntrada() {
		return DTEntrada;
	}
	public void setDTEntrada(Date dTEntrada) {
		DTEntrada = dTEntrada;
	}
	public Date getDTSaida() {
		return DTSaida;
	}
	public void setDTSaida(Date dTSaida) {
		DTSaida = dTSaida;
	}
	public Double getValor() {
		return Valor;
	}
	public void setValor(Double valor) {
		Valor = valor;
	}
	public int getTipoPagamento() {
		return TipoPagamento;
	}
	public void setTipoPagamento(int tipoPagamento) {
		TipoPagamento = tipoPagamento;
	}
	public Chale getChale() {
		return chale;
	}
	public void setChale(Chale chale) {
		this.chale = chale;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
    
}
