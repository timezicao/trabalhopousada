package br.com.control;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dao.IEnderecoDAO;
import br.com.dao.IFuncionarioDAO;
import br.com.dao.impl.EnderecoDAOImpl;
import br.com.dao.impl.FuncionarioDAOImpl;
import br.com.model.Endereco;
import br.com.model.Funcionario;
import br.com.model.TipoFuncionario;
import br.com.singleton.FactorySingleton;

@WebServlet("/funcionarioServlet")
public class FuncionarioServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	EntityManager em = FactorySingleton.getInstance().createEntityManager();
	IFuncionarioDAO fdao = new FuncionarioDAOImpl(em);
	IEnderecoDAO edao = new EnderecoDAOImpl(em);
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		listar(req);
		doPost(req, resp);
	}


	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String retorno = "";
		String acao = req.getParameter("acao");
		
		switch (acao) {
		case "cadastrar":
			cadastrar(req);
			retorno = "funcionarioServlet?acao=listar";
			break;
		case "excluir":
			excluir(req);
			listar(req);// busca a lista
			retorno = "index.jsp";
			break;
		case "listar":
			listar(req);
			retorno = "index.jsp";
			break;
		case "carregar":
			// abre a pagina com o formulario preenchido
			listar(req);
			carregar(req);
			retorno = "alterarFuncionario.jsp";
			break;
		case "alterar":
			try {
				alterar(req);
				listar(req);
				retorno = "funcionarioServlet?acao=listar";
				
			} catch (Exception e) {

				
				e.printStackTrace();
			}
			
			break;
		
		default:
			break;
		}
		
		// redirecionar para a pagina cadastrar
		req.getRequestDispatcher(retorno).forward(req, resp);
		
	}


	private void carregar(HttpServletRequest req) {
		// TODO Auto-generated method stub
		int codigo = Integer.parseInt(req.getParameter("codigo"));
		Funcionario func = new Funcionario();
		func = fdao.searchById(codigo);
		
		System.out.println(func.getEndereco().getRua());
		//coloca o funcionario no request
		req.setAttribute("funcionario", func);
	}


	private void alterar(HttpServletRequest request) {
		// TODO Auto-generated method stub
		
		Funcionario f = new Funcionario();
		Endereco e = new Endereco();
		
		f.setIdUsuario(Integer.parseInt(request.getParameter("codigo")));
		e.setIdEndereco(Integer.parseInt(request.getParameter("codEndereco")));
		f.setNome(request.getParameter("nome"));
		f.setSobreNome(request.getParameter("sobreNome"));
		f.setRg(request.getParameter("rg"));
		f.setCpf(request.getParameter("cpf"));
		f.setCelular(request.getParameter("celular"));
		f.setSalario(Double.parseDouble(request.getParameter("salario")));
		e.setRua(request.getParameter("rua"));
		e.setNumero(Integer.parseInt(request.getParameter("numero")));
		e.setCep(request.getParameter("cep"));
		
		Date dataNasc = null;
		try {
			dataNasc = new SimpleDateFormat("dd/MM/yyyy").parse(request.getParameter("dataNascimento"));
		} catch (ParseException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		} 
		f.setDataNascimento(dataNasc);
		f.setTelefoneResidencial(request.getParameter("telefoneResidencial"));
		f.setFuncao(Enum.valueOf(TipoFuncionario.class,request.getParameter("funcao")));
		
		Date dataAdmissao = null;
		try {
			dataAdmissao = new SimpleDateFormat("dd/MM/yyyy").parse(request.getParameter("dataAdmissao"));
		} catch (ParseException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		} 
		f.setDataAdmissao(dataAdmissao);
		e.setBairro(request.getParameter("bairro"));
		e.setCidade(request.getParameter("cidade"));
		e.setEstado(request.getParameter("estado"));
		f.setEndereco(e);
		
		fdao.update(f);
		edao.update(e);
	}


	private void excluir(HttpServletRequest req) {
		// TODO Auto-generated method stub
		int codigo = Integer.parseInt(req.getParameter("codigo"));
		fdao.remove(codigo);
	}


	private void cadastrar(HttpServletRequest request){
		
		Funcionario f = new Funcionario();
		Endereco e = new Endereco();
		
		f.setNome(request.getParameter("nome"));
		f.setSobreNome(request.getParameter("sobreNome"));
		f.setRg(request.getParameter("rg"));
		f.setCpf(request.getParameter("cpf"));
		f.setCelular(request.getParameter("celular"));
		f.setSalario(Double.parseDouble(request.getParameter("salario")));
		e.setRua(request.getParameter("rua"));
		e.setNumero(Integer.parseInt(request.getParameter("numero")));
		e.setCep(request.getParameter("cep"));
		
		Date dataNasc = null;
		try {
			dataNasc = new SimpleDateFormat("dd/MM/yyyy").parse(request.getParameter("dataNascimento"));
		} catch (ParseException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		} 
		f.setDataNascimento(dataNasc);
		f.setTelefoneResidencial(request.getParameter("telefoneResidencial"));
		
		//System.out.println(request.getParameter("funcao") + " teste servlet"); 
		
		f.setFuncao(Enum.valueOf(TipoFuncionario.class,request.getParameter("funcao")));
		
		Date dataAdmissao = null;
		try {
			dataAdmissao = new SimpleDateFormat("dd/MM/yyyy").parse(request.getParameter("dataAdmissao"));
		} catch (ParseException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		} 
		f.setDataAdmissao(dataAdmissao);
		e.setBairro(request.getParameter("bairro"));
		e.setCidade(request.getParameter("cidade"));
		e.setEstado(request.getParameter("estado"));
		f.setEndereco(e);
		
		edao.insert(e);
		fdao.insert(f);
		
	}
	
	private void listar(HttpServletRequest req) {
		
		List<Funcionario> lista = fdao.listarTodos();
		req.setAttribute("listaFuncionarios", lista);
		
	}

}
