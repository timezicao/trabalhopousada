package br.com.control;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dao.IChaleDAO;
import br.com.dao.IClienteDAO;
import br.com.dao.IReservaDAO;
import br.com.dao.impl.ChaleDAOImpl;
import br.com.dao.impl.ClienteDAOImpl;
import br.com.dao.impl.ReservaDAOImpl;
import br.com.singleton.FactorySingleton;

@WebServlet("/reservaServlet")
public class ReservaServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	EntityManager em = FactorySingleton.getInstance().createEntityManager();
	IReservaDAO rdao = new ReservaDAOImpl(em);
	IClienteDAO cdao = new ClienteDAOImpl(em);
	IChaleDAO chdao = new ChaleDAOImpl(em);
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		listar(req);
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
			
		String retorno = "";
		String acao = req.getParameter("acao");
		
		switch (acao) {
		case "cadastrar":
			cadastrar(req);
			retorno = "reservaServlet?acao=listar";
			break;
		case "excluir":
			excluir(req);
			listar(req);// busca a lista
			retorno = "crudReserva.jsp";
			break;
		case "listar":
			listar(req);
			retorno = "crudReserva.jsp";
			break;
		case "carregar":
			// abre a pagina com o formulario preenchido
			listar(req);
			carregar(req);
			retorno = "alterarFuncionario.jsp";
			break;
		case "alterar":
			try {
				alterar(req);
				listar(req);
				retorno = "reservaServlet?acao=listar";
				
			} catch (Exception e) {

				
				e.printStackTrace();
			}
			
			break;
		
		default:
			break;
		}
		// redirecionar para a pagina cadastrar
		req.getRequestDispatcher(retorno).forward(req, resp);
	}
	
	private void alterar(HttpServletRequest req) {
		// TODO Auto-generated method stub
		
	}

	private void carregar(HttpServletRequest req) {
		// TODO Auto-generated method stub
		
	}

	private void excluir(HttpServletRequest req) {
		// TODO Auto-generated method stub
		
	}

	private void cadastrar(HttpServletRequest req) {
		// TODO Auto-generated method stub
		
	}

	private void listar(HttpServletRequest req) {
		// TODO Auto-generated method stub
		
	}

}
