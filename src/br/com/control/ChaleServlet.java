package br.com.control;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dao.IChaleDAO;
import br.com.dao.impl.ChaleDAOImpl;
import br.com.model.Chale;
import br.com.model.Funcionario;
import br.com.model.TipoChale;
import br.com.singleton.FactorySingleton;

/**
 * Servlet implementation class ChaleServlet
 */
@WebServlet("/ChaleServlet")
public class ChaleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    EntityManager em = FactorySingleton.getInstance().createEntityManager();
    IChaleDAO cdao = new ChaleDAOImpl(em);
    
    
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		listar(req);
		doPost(req, resp);
	}
    
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String retorno = "";
		String acao = request.getParameter("acao");
		
		switch(acao){
		case "cadastrar":
			cadastrar(request);
			listar(request);
			retorno = "crudChale.jsp";
			break;		
			
		case "excluir":
			excluir(request);
			listar(request);// busca a lista
			retorno = "crudChale.jsp";
			break;
		case "carregar":
			// abre a pagina com o formulario preenchido
			listar(request);
			carregar(request);
			retorno = "alterarChale.jsp";
			break;
		case "listar":
			listar(request);
			retorno = "crudChale.jsp";
			break;
		case "alterar":
			try {
				alterar(request);
				listar(request);
				retorno = "crudChale.jsp";				
			} catch (Exception e) {				
				e.printStackTrace();
			}
			
			break;
		}
		
		request.getRequestDispatcher(retorno).forward(request,response);
	}
	
	private void excluir(HttpServletRequest req) {
		// TODO Auto-generated method stub
		int codigo = Integer.parseInt(req.getParameter("codigo"));
		cdao.remove(codigo);
	}
	
	private void alterar(HttpServletRequest request){
		Chale c = new Chale();
		
		c.setNumChale(Integer.parseInt(request.getParameter("numChale")));
		c.setIDChale(Integer.parseInt(request.getParameter("codigo"))); 
		
		c.setCamaOpcional(request.getParameter("camaOpc"));
		c.setTipoChale(Enum.valueOf(TipoChale.class,request.getParameter("tipoChale")));
		c.setValor_diaria(Double.parseDouble(request.getParameter("valorChale")));		
		
		cdao.update(c);
	}
	
	private void listar(HttpServletRequest request) {
		
		List<Chale> lista = cdao.listarTodos();
		request.setAttribute("listaChale", lista);
		
	}
	
	private void carregar(HttpServletRequest req) {
	// TODO Auto-generated method stub
	int codigo = Integer.parseInt(req.getParameter("codigo"));
	Chale c = new Chale();
	c = cdao.searchById(codigo);
	
	//coloca o chal� no request
	req.setAttribute("chale", c);
} 
	

	private void cadastrar(HttpServletRequest request){
		Chale c = new Chale();
		
		c.setNumChale(Integer.parseInt(request.getParameter("numChale")));
		c.setCamaOpcional(request.getParameter("camaOpc"));
		c.setTipoChale(Enum.valueOf(TipoChale.class,request.getParameter("tipoChale")));
		c.setValor_diaria(Double.parseDouble(request.getParameter("valorChale")));
		
		
		cdao.insert(c);
	}

}
