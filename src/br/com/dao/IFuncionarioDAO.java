package br.com.dao;

import java.util.List;

import br.com.model.Funcionario;

public interface IFuncionarioDAO extends DAO<Funcionario,Integer>{

	List<Funcionario> listarTodos();
}
