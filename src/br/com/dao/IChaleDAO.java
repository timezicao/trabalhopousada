package br.com.dao;

import java.util.List;

import br.com.model.Chale;

public interface IChaleDAO extends DAO<Chale, Integer> {
	
	List<Chale> listarTodos();
}
