package br.com.dao;

import java.util.List;

import br.com.model.Cliente;

public interface IClienteDAO extends DAO<Cliente, Integer > {
	
	List<Cliente> listarTodos();
}
