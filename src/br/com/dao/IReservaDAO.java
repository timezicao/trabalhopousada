package br.com.dao;

import java.util.List;

import br.com.model.Reserva;

public interface IReservaDAO extends DAO<Reserva,Integer>{
	
	List<Reserva> listarTodos();
}
