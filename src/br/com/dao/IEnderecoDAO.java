package br.com.dao;

import java.util.List;

import br.com.model.Endereco;

public interface IEnderecoDAO extends DAO<Endereco,Integer>{

	List<Endereco> listarTodos();
}
