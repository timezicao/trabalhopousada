package br.com.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.dao.IClienteDAO;
import br.com.model.Cliente;

public class ClienteDAOImpl extends DAOImpl<Cliente, Integer> implements IClienteDAO {

	public ClienteDAOImpl(EntityManager em) {
		super(em);
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Cliente> listarTodos() {
		TypedQuery<Cliente> query = em.createQuery(" from Cliente", Cliente.class);
		return query.getResultList();
	}

}
