package br.com.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.dao.IReservaDAO;
import br.com.model.Reserva;

public class ReservaDAOImpl extends DAOImpl<Reserva, Integer> implements IReservaDAO {

	public ReservaDAOImpl(EntityManager em) {
		super(em);
		// TODO Auto-generated constructor stub
	}
	
	public List<Reserva> listarTodos() {
		// TODO Auto-generated method stub
		TypedQuery<Reserva> query = em.createQuery("from Reserva", Reserva.class);
		return query.getResultList();
	}

}
