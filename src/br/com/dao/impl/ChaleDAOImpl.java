package br.com.dao.impl;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.Join;

import br.com.dao.IChaleDAO;
import br.com.model.Chale;

public class ChaleDAOImpl extends DAOImpl<Chale, Integer> implements IChaleDAO {

	public ChaleDAOImpl(EntityManager em) {
		super(em);
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Chale> listarTodos() {
		// TODO Auto-generated method stub
		TypedQuery<Chale> query = em.createQuery(" from Chale", Chale.class);
		return query.getResultList();
		
	}
	
	

}
