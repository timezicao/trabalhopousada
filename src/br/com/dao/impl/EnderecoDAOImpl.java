package br.com.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.dao.IEnderecoDAO;
import br.com.model.Endereco;

public class EnderecoDAOImpl extends DAOImpl<Endereco,Integer> implements IEnderecoDAO{

	public EnderecoDAOImpl(EntityManager em) {
		super(em);
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Endereco> listarTodos() {
		// TODO Auto-generated method stub
		TypedQuery<Endereco> query = em.createQuery("from Endereco", Endereco.class);
		return query.getResultList();
	}

}
