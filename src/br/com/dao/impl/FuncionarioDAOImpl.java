package br.com.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.dao.IFuncionarioDAO;
import br.com.model.Funcionario;

public class FuncionarioDAOImpl extends DAOImpl<Funcionario, Integer> implements IFuncionarioDAO {

	public FuncionarioDAOImpl(EntityManager em) {
		super(em);
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Funcionario> listarTodos() {
		// TODO Auto-generated method stub
		TypedQuery<Funcionario> query = em.createQuery("from Funcionario", Funcionario.class);
		return query.getResultList();
	}

}
